#!/usr/bin/python

import json


def process_json_file():
    lines = []

    with open('./resources/data.json') as json_file:
        device_dictionary = json.load(json_file)

    device_list = device_dictionary['devices']

    for this_device in device_list:
        uuid = this_device['uuid']
        lat = ("% .5f" % this_device['lat']).strip()
        long = ("%.5f" % this_device['long']).strip()
        timestamp = str(this_device['timestamp'])

        device_string = uuid+','+lat+','+long+','+timestamp

        lines.append(device_string.encode('ascii', 'ignore'))

    return lines


def process_csv_file():
    with open('./resources/data.csv') as csv_file:
        lines = csv_file.read().splitlines()

    # delete column heading string
    lines.pop(0)
    return lines


def remove_duplicates_from_list(l):
    return list(set(l))


def create_master_device_list():
    device_list = process_csv_file() + process_json_file()
    device_list = remove_duplicates_from_list(device_list)
    return device_list


def calculate_statistics(device_list):
    uuid_list = []
    within_geo_area_list = []

    oldest_update_time = 9999999999
    newest_update_time = 0

    for device_string in device_list:
        fields = device_string.split(',')
        uuid = fields[0]
        lat_float = float(fields[1])
        long_float = float(fields[2])
        timestamp_int = int(fields[3])

        uuid_list.append(uuid)

        if timestamp_int > newest_update_time:
            newest_update_time = timestamp_int
            newest_update_time_uuid = uuid

        if timestamp_int < oldest_update_time:
            oldest_update_time = timestamp_int
            oldest_update_time_uuid = uuid

        if (((long_float > -179.99) and (long_float < 0.00)) and
                ((lat_float > 0.00) and (lat_float < 89.99))):
            within_geo_area_list.append(uuid)

    total_number_of_devices = len(uuid_list)
    number_unique_devices = len(set(uuid_list))
    within_geo_area_unique_sorted = sorted(list(set(within_geo_area_list)))
    number_devices_within_area = len(within_geo_area_unique_sorted)

    out_json = {
        "total number of devices": total_number_of_devices,
        "total unique devices": number_unique_devices,
        "device ID with oldest update time": [oldest_update_time_uuid, oldest_update_time],
        "device ID with newest update time": [newest_update_time_uuid, newest_update_time],
        "number of devices within the geographic area": [number_devices_within_area, within_geo_area_unique_sorted]
    }

    results_json = {"Results": out_json}

    with open('./results.json', 'w') as outfile:
        json.dump(results_json, outfile, sort_keys=True, indent=4, ensure_ascii=False)

    
master_device_list = create_master_device_list()
calculate_statistics(master_device_list)
